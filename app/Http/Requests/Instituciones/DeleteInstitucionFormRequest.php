<?php

namespace App\Http\Requests\Instituciones;

use Illuminate\Foundation\Http\FormRequest;

class DeleteInstitucionFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public
    function rules()
    {
        return [
            'id' => ['required', 'exists:instituciones,id'],
        ];
    }

    public function messages()
    {
        return [
            'id.required' => 'Id de institucion requerido',
            'id.exists' => 'La institucion no existe',
        ];
    }
}
