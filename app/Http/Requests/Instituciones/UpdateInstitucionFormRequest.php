<?php

namespace App\Http\Requests\Instituciones;

use Illuminate\Foundation\Http\FormRequest;

class UpdateInstitucionFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public
    function rules()
    {
        return [
            'id' => ['required', 'exists:instituciones,id'],
            'cuise' => ['required', "unique:instituciones,cuise,{$this->id},id,deleted_at,NULL", 'digits:5'],
            'institucion' => ['required', "unique:instituciones,institucion,{$this->id},id,deleted_at,NULL"],
        ];
    }

    public function messages()
    {
        return [
            'id.required' => 'Id de institucion requerido',
            'id.exists' => 'Id no existe',
            'institucion.required' => 'Ingrese nombre de institucion',
            'institucion.unique' => 'Nombre ya existe',
            'cuise.unique' => 'CUISE ya existe',
            'cuise.required' => 'CUISE requerido',
            'cuise.digits' => 'CUISE debe ser de 5 digitos',
        ];
    }
}
