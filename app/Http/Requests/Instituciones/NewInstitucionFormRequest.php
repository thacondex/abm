<?php

namespace App\Http\Requests\Instituciones;

use Illuminate\Foundation\Http\FormRequest;

class NewInstitucionFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public
    function rules()
    {
        return [
            'cuise' => ['required', 'unique:instituciones,cuise,NULL,id,deleted_at,NULL', 'digits:5'],
            'institucion' => ['required', 'unique:instituciones,institucion,NULL,id,deleted_at,NULL'],
            // 'direccion' => ['required'],
        ];
    }

    public function messages()
    {
        return [
            'institucion.required' => 'Ingrese nombre de institucion',
            'institucion.unique' => 'El nombre ya existe',
            'cuise.required' => 'CUISE requerido',
            'cuise.unique' => 'El CUISE ya existe',
            'cuise.digits' => 'CUISE debe ser de 5 digitos',
        ];
    }
}
