<?php

namespace App\Http\Controllers;

use App\Http\Requests\Instituciones\DeleteInstitucionFormRequest;
use App\Http\Requests\Instituciones\UpdateInstitucionFormRequest;
use App\Institucion;
use App\Http\Requests\Instituciones\NewInstitucionFormRequest;
use App\Utils\APIResponse;
use Illuminate\Support\Facades\DB;

class InstitucionesController extends Controller
{

    public function get()
    {
        try {
            $instituciones = Institucion::all();
            return APIResponse::customResponse('1', 200, '', $instituciones, '', '');
        } catch (\Exception $e) {
            return APIResponse::customResponse('0', 500, 'Algo falló', '', $e->getMessage(), $e->getMessage());
        }
    }

    public function store(NewInstitucionFormRequest $request)
    {
        DB::beginTransaction();
        try {
            $nuevaInstitucion = Institucion::create([
                'cuise' => $request->cuise,
                'institucion' => $request->institucion,
                'direccion' => $request->direccion,
            ]);
            $datos = $nuevaInstitucion;
            DB::commit();
            return APIResponse::customResponse('1', 200, '', $datos, '', '');
        } catch (\Exception $e) {
            DB::rollback();
            return APIResponse::customResponse('0', 500, 'Algo falló', '', $e->getMessage(), $e->getMessage());
        }
    }

    public function update(UpdateInstitucionFormRequest $request)
    {
        DB::beginTransaction();
        try {
            $institucion = Institucion::find($request->id);
            $institucion->update([
                'cuise' => $request->cuise,
                'institucion' => $request->institucion,
                'direccion' => $request->direccion
            ]);
            $datos = $institucion;
            DB::commit();
            return APIResponse::customResponse('1', 200, '', $datos, '', '');
        } catch (\Exception $e) {
            DB::rollback();
            return APIResponse::customResponse('0', 500, 'Algo falló', '', $e->getMessage(), $e->getMessage());
        }
    }

    public function delete(DeleteInstitucionFormRequest $request)
    {
        DB::beginTransaction();
        try {
            $institucion = Institucion::find($request->id);
            $institucion->delete();
            DB::commit();
            return APIResponse::customResponse('1', 200, '', '', '', '');
        } catch (\Exception $e) {
            DB::rollback();
            return APIResponse::customResponse('0', 500, 'Algo falló', '', $e->getMessage(), $e->getMessage());
        }
    }
}
