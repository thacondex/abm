<?php

namespace App;

class Institucion extends BaseModel
{
    protected $table = 'instituciones';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'cuise',
        'institucion', 'direccion'
    ];
    protected $guarded = [
        'id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        //  'deleted_at',
        'created_at',
        'updated_at',
    ];
}
