<?php

namespace App\Utils;

// Esta clase sirve para manejar las respuestas que son enviadas al cliente de forma conveniente
class APIResponse
{

    public static function customResponse($state, $code, $message, $data, $exception,$errors)
    {
        $data = collect([
            "state" => $state,
            "code" => $code,
            "message" => $message,
            "data" => $data,
            "exception" => $exception,
            "errors"=>$errors
        ]);
        return response($data, $code);
    }
}

?>