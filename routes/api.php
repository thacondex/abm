<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;


Route::group(['prefix' => 'v1'], function () {
    Route::group(['prefix' => 'instituciones'], function () {
        Route::get('get', 'InstitucionesController@get');
        Route::delete('delete', 'InstitucionesController@delete');
        Route::post('create', 'InstitucionesController@store');
        Route::put('update', 'InstitucionesController@update');
    });
});
