import Vue from 'vue';
import Vuex from 'vuex';
import instituciones from '../app/instituciones/vuex';
import helpers from '../app/helpers/vuex';

Vue.use(Vuex);

export default new Vuex.Store({
    modules: {
        instituciones: instituciones,
        helpers: helpers
    }
})