import Vue from 'vue'
export const YesNoDialog = Vue.component('yesnodialog', require('./YesNoDialog').default);
export const CardCustomHeader = Vue.component('cardcustomheader', require('./CardCustomHeader').default);