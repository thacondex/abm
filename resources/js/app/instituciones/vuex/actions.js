//INSTITUCIONES
export const fetchInstituciones = ({ commit }) => {
    return loadingAxiosInstance.get(api.instituciones().get).then((response) => {
        commit('setInstituciones', response.data.data);
    }).catch((error) => {
        return Promise.reject(error.response.data.errors);
    })
};
export const updateInstitucion = ({ commit }, [InstitucionModificada, index]) => {
    return loadingAxiosInstance.put(api.instituciones().update, InstitucionModificada).then((response) => {
        commit('updateInstitucion', [response.data.data, index]);
    }).catch((error) => {
        return Promise.reject(error.response.data.errors);
    })
};
export const createInstitucion = ({ commit }, [institucion]) => {
    return loadingAxiosInstance.post(api.instituciones().create, institucion).then((response) => {
        commit('appendInstitucion', [response.data.data]);
    }).catch((error) => {
        return Promise.reject(error.response.data.errors);
    })
};
export const deleteInstitucion = ({ commit }, institucion) => {
    return loadingAxiosInstance.delete(api.instituciones().delete, { data: { id: institucion.id } }).then((response) => {
        commit('removeInstitucion', institucion)
    }).catch((error) => {
        return Promise.reject(error.response.data.errors);
    })
};