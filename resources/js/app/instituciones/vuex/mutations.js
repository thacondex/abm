//INSTITUCIONES
export const setInstituciones = (state, data) => {
    state.instituciones = data
}
export const updateInstitucion = (state, [institucion, index]) => {
    state.instituciones[index].cuise = institucion.cuise
    state.instituciones[index].institucion = institucion.institucion
    state.instituciones[index].direccion = institucion.direccion
}
export const appendInstitucion = (state, data) => {
    state.instituciones.push(Object.assign({}, data)[0])
}
export const removeInstitucion = (state, institucion) => {
    state.instituciones.splice(state.instituciones.indexOf(institucion), 1)
}