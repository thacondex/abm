import {
    Instituciones,
} from '../components'

export default [
    {
        path: '/instituciones/',
        component: Instituciones,
        name: 'instituciones',
        meta: {
            guest: false,
            needsAuth: false
        },
    }
]
