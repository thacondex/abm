/**
 * Created by emill on 2020-03-15.
 */
import home from './home/routes';
import instituciones from './instituciones/routes';
import errors from './errors/routes';

export default [...home, ...instituciones, ...errors]