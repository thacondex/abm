export const setSnackbar = (state, data) => {
    state.snackbar.active = data.active;
    state.snackbar.message = data.message;
    state.snackbar.color = data.color;
};

export const setLoading = (state, data) => {
    state.loading = data
};
