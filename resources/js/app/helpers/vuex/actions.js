export const activeSnackbar = ({commit},[message,color]) => {
    commit('setSnackbar', {active:true,message:message,color:color});
};