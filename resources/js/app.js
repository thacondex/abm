import router from './router';
import store from './vuex';
import vuetify from './plugins/vuetify';
import { API, loadingAxiosInstance, commonAxiosInstance } from './helpers';

require('./bootstrap');

//CONSTANTE PARA LAS URLS DE LA API
window.api = API;
window.loadingAxiosInstance = loadingAxiosInstance;
window.commonAxiosInstance = commonAxiosInstance;
window.store = store;
window.router = router;


window.Vue = require('vue');

Vue.component('app', require('./components/App.vue').default);
Vue.component('toolbar', require('./components/Toolbar.vue').default);
Vue.component('navigation', require('./components/Navigation.vue').default);
Vue.component('foot', require('./components/Foot.vue').default);
Vue.component('loading', require('./components/Loading.vue').default);

const app = new Vue({
    vuetify,
    router: router,
    store: store,
    el: '#app'
})

