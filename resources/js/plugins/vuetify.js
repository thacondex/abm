import Vue from 'vue';
import Vuetify from 'vuetify';
import 'vuetify/dist/vuetify.min.css';


//CUSTOM COMPONENTS
Vue.component('cardCustomHeader', require('../components/custom/CardCustomHeader.vue').default)

const components = {};
const directives = {};
Vue.use(Vuetify);
export default new Vuetify({
    icons: { iconfont: 'mdi' },
});