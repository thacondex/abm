import axios from 'axios';
import store from '../vuex';


//INSTANCIAS PARA EL USO DE AXIOS
export const loadingAxiosInstance = axios.create({
    baseURL: '/api/v1'
});
export const commonAxiosInstance = axios.create({
    baseURL: '/api/v1'
});


//ESTO SE EJECUTA ANTES DE LA REQUEST, SE HACE PARA PODER TENER EL LOADING DE FORMA GLOBAL
// Add a request interceptor
loadingAxiosInstance.interceptors.request.use(function (config) {
    store.commit('helpers/setLoading', true);
    return config;
}, function (error) {
    store.commit('helpers/setLoading', false);
    return Promise.reject(error);
});

//ESTO SE EJECUTA DESPUES DE LA REQUEST
// Add a response interceptor
loadingAxiosInstance.interceptors.response.use(function (response) {
    // Any status code that lie within the range of 2xx cause this function to trigger
    store.commit('helpers/setLoading', false);
    return response;
}, function (error) {
    // Any status codes that falls outside the range of 2xx cause this function to trigger
    store.commit('helpers/setLoading', false);
    return Promise.reject(error);
});
commonAxiosInstance.interceptors.response.use(function (response) {
    return response;
}, function (error) {
    return Promise.reject(error);
});


//CONSTANTES PARA PATHS
const institucionesPath = '/instituciones/';

export default {
    instituciones() {
        return {
            get: institucionesPath + 'get',
            update: institucionesPath + 'update',
            create: institucionesPath + 'create',
            delete: institucionesPath + 'delete',
        }
    },
}