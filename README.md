NODE VERSION 12.16.1
NPM VERSION 6.13.4

AFTER CLONING

<ul>
	<li>CREATE .ENV FILE</li>
	<li>RUN "PHP ARTISAN KEY:GENERATE"</li>
	<li>CHANGE CONNECTION PARAMETERS IN .ENV</li>
	<li>RUN "PHP ARTISAN MIGRATE"</li>
	<li>"PHP ARTISAN SERV" TO RUN THE SERVER</li>
</ul>

IF YOU WANT TO MAKE CHANGES TO THE VUE FRONT-END

<ul>
	<li>RUN "NPM INSTALL" TO INSTALL THE NODE_MODULES</li>
	<li>MAKE CHANGES</li>
	<li>RUN "NPM RUN DEV" OR "NPM RUN PRODUCTION"</li>
</ul>

## BACK-END STRUCTURE

<ul>
	<li>Controllers in "App/Http/Controllers".</li>
	<li>Validations in "App/Http/Requests/Instituciones".</li>
	<li>Routes in "Routes/api.php".</li>
	<li>Models in "App"</li>
</ul>

## FRONT-END STRUCTURE

<ul>
	<li>Main folder in "resources/js".</li>
	<li>Main file "resources/js/app.js".</li>
	<li>Each section has its own folder in "resources/js/app".</li>
	<li>Each section contains 3 subfolders: components, routes, and vuex.</li>
	<li>Components folders contain the main vue files for each section.</li>
	<li>Routes folder contains a file that links routes and components.</li>
	<li>Vuex folder contains vuex storage files.</li>
</ul>
